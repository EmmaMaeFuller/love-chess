
local function index_to_coord(index) return (index - 1) % Cols, math.floor((index - 1) / Rows) end
local function coord_to_index(col, row) return (col + 1) + (row * Cols) end

local function prepare_move_ray(moves, col, row)
    return function(offsets)
        for i in ipairs(offsets) do
            for j=1,8 do
                local square = { col + (j * offsets[i][1]), row + (j * offsets[i][2]) }
                
                table.insert(moves, square)
                if Squares[coord_to_index(square[1], square[2])] ~= Empty then goto piecehit end
            end
            ::piecehit::
        end
    end
end

local function get_possible_moves(piece, col, row)
    local unfiltered_moves = {}

    if piece == Empty then return unfiltered_moves end

    if piece.sprite == Pawn then
        local direction = piece.color[1] == 0 and 1 or -1
        table.insert(unfiltered_moves, { col, row + (1 * direction) })
        if not piece.moved then table.insert(unfiltered_moves, { col, row + (2 * direction) }) end
        -- TODO: Add en passant and capture rules
    end

    -- TODO: add castling

    local send_move_ray = prepare_move_ray(unfiltered_moves, col, row)

    if piece.sprite == Rook or piece.sprite == Queen then
        send_move_ray({ {  0, -1 }, {  0,  1 }, { -1,  0 }, {  1,  0 } })
    end

    if piece.sprite == Bishop or piece.sprite == Queen then
        send_move_ray({ { -1, -1 }, {  1,  1 }, {  1, -1 }, { -1,  1 } })
    end

    if piece.sprite == Knight then
        for _, i in ipairs({ -2, -1, 1, 2 }) do
            for _, j in ipairs({ -2, -1, 1, 2 }) do
                if math.abs(i) ~= math.abs(j) then table.insert(unfiltered_moves, { col + i, row + j }) end
            end
        end
    end

    if piece.sprite == King then
        for i=-1,1 do
            for j=-1,1 do
                if i ~= 0 or j ~= 0 then
                    table.insert(unfiltered_moves, { col + i, row + j })
                end
            end
        end
    end

    local moves = {}
    for _, move in ipairs(unfiltered_moves) do
        local other = Squares[coord_to_index(move[1], move[2])]

        local is_friendly_fire = (other ~= Empty and other ~= nil and piece.color[1] == other.color[1])
        local is_out_of_bounds = move[1] < 0 or move[2] < 0 or move[1] > Cols - 1 or move[2] > Rows - 1

        if not is_friendly_fire and not is_out_of_bounds then
            table.insert(moves, move)
        end
    end

    return moves
end

local function get_hovered_square()
    local x, y = love.mouse.getPosition()
    return math.floor(x / TileSize), math.floor(y / TileSize)
end

function love.load()
    Rows = 8
    Cols = 8
    TileSize = 60
    Tiles = Rows * Cols - 1
    HasFocus = false

    -- There's nothing special about the string "empty",
    -- but lua iterators are nil terminated so i need to use
    -- a sentinal value to indicate if a square is empty
    Empty = "nothing"
    Selected = nil

    Current = 1

    Pawn = love.graphics.newImage("pieces/pawn.png")
    Rook = love.graphics.newImage("pieces/rook.png")
    Knight = love.graphics.newImage("pieces/knight.png")
    Bishop = love.graphics.newImage("pieces/bishop.png")
    Queen = love.graphics.newImage("pieces/queen.png")
    King = love.graphics.newImage("pieces/king.png")

    local function dark(sprite) return { color = { 0, 0, 0 }, sprite = sprite, moved = false } end
    local function light(sprite) return { color = { 1, 1, 1 }, sprite = sprite, moved = false } end

    -- CCCCCCCC
    Squares = {
        dark(Rook),     dark(Knight),   dark(Bishop),   dark(Queen),    dark(King),     dark(Bishop),   dark(Knight),   dark(Rook),
        dark(Pawn),     dark(Pawn),     dark(Pawn),     dark(Pawn),     dark(Pawn),     dark(Pawn),     dark(Pawn),     dark(Pawn),
        Empty,          Empty,          Empty,          Empty,          Empty,          Empty,          Empty,          Empty,
        Empty,          Empty,          Empty,          Empty,          Empty,          Empty,          Empty,          Empty,
        Empty,          Empty,          Empty,          Empty,          Empty,          Empty,          Empty,          Empty,
        Empty,          Empty,          Empty,          Empty,          Empty,          Empty,          Empty,          Empty,
        light(Pawn),    light(Pawn),    light(Pawn),    light(Pawn),    light(Pawn),    light(Pawn),    light(Pawn),    light(Pawn),
        light(Rook),    light(Knight),  light(Bishop),  light(Queen),   light(King),    light(Bishop),  light(Knight),  light(Rook),
    }
end

function love.draw()
    for i, piece in ipairs(Squares) do
        local col, row = index_to_coord(i)
        -- Draw tiles
        love.graphics.setColor((row + col) % 2 == 0 and { 0.4, 0.4, 0.4 } or { 0.8, 0.8, 0.8 })
        love.graphics.rectangle("fill", col * TileSize, row * TileSize, TileSize, TileSize)

        if piece ~= Empty then
            if Selected and Selected.piece == piece then
                -- Ghost of currently selected piece
                love.graphics.setColor({ piece.color[1], piece.color[2], piece.color[3], 0.25 })
            else
                -- Regular piece
                love.graphics.setColor(piece.color)
            end

            -- Draw piece
            love.graphics.draw(piece.sprite, col * TileSize, row * TileSize)
        end
    end

    if HasFocus then
        -- Draw selected piece next to cursor
        if Selected then
            local x, y = love.mouse.getPosition()
            love.graphics.setColor(Selected.piece.color)
            love.graphics.draw(Selected.piece.sprite, x + 5, y + 5, 0, .5, .5)
        end

        local col, row = get_hovered_square()

        -- Draw highlight on square being hovered by mouse
        love.graphics.setColor(0, 0, 1, 0.15)
        love.graphics.rectangle("fill", col * TileSize, row * TileSize, TileSize, TileSize)

        -- Draw highlight on legal moves based on hovered/selected piece
        local legal_moves
        local target_square, target_col, target_row
        if Selected then
            target_square, target_col, target_row = Selected.piece, Selected.col, Selected.row
        else
            target_square, target_col, target_row = Squares[coord_to_index(col, row)], col, row
        end
        if target_square ~= Empty and Current == target_square.color[1] then
            legal_moves = get_possible_moves(target_square, target_col, target_row)
            love.graphics.setColor(0, 1, 0, 0.15)
            for _, move in ipairs(legal_moves) do
                if Squares[coord_to_index(move[1], move[2])] == Empty then
                    love.graphics.setColor(0, 1, 0, 0.15)
                else
                    love.graphics.setColor(1, 0, 0, 0.15)
                end
                love.graphics.rectangle("fill", move[1] * TileSize, move[2] * TileSize, TileSize, TileSize)
            end
        end
    end
end

function love.mousefocus(f)
    HasFocus = f
    if not HasFocus then
        Selected = nil
    end
end

function love.mousepressed()
    local col, row = get_hovered_square()
    local piece = Squares[coord_to_index(col, row)]

    if piece ~= Empty and piece.color[1] == Current then
        Selected = { col = col, row = row, piece = piece }
    end
end

function love.mousereleased()
    if Selected == nil then return end

    local col, row = get_hovered_square()

    -- Only move the piece if it lands on a legal square
    local legal_moves = get_possible_moves(Selected.piece, Selected.col, Selected.row)
    for _, move in ipairs(legal_moves) do
        if move[1] == col and move[2] == row then
            Selected.piece.moved = true

            ---@diagnostic disable-next-line: assign-type-mismatch
            Squares[coord_to_index(Selected.col, Selected.row)] = Empty
            Squares[coord_to_index(col, row)] = Selected.piece

            if Current == 0 then Current = 1 else Current = 0 end
            break
        end
    end

    Selected = nil
end